from django.urls import path, include
from . import views



urlpatterns = [
# post views
path('list_prof/', views.listof_prof, name='list_prof'),
path('list_prof/view_prof/<int:id>', views.view_prof, name='view_prof'),

]