from django.contrib import admin
from django.urls import path, include
from django.conf import settings
from django.conf.urls import url
from django.conf.urls.static import static
from django.contrib.auth import views as auth_views

urlpatterns = [
    path('admin/', admin.site.urls),
	path('voir/', include('voir.urls')),
    path('account/', include('account.urls')),
    path('professeur/', include('professeur.urls')),
    path('etudiant/', include('etudiant.urls')),
    path('sujetmemoire/', include('sujetmemoire.urls')),
    path('extra/', include('extra.urls')),
    url(r'^', include('crud.urls')),
]

if settings.DEBUG:
    urlpatterns += static(settings.MEDIA_URL,document_root=settings.MEDIA_ROOT)