from django.urls import path, include
from . import views



urlpatterns = [
# post views
path('list_etudiant/', views.listof_etudiant, name='list_etudiant'),
path('list_etudiant/view_etudiant/<int:id>', views.view_etudiant, name='view_etudiant'),
]