from django.apps import AppConfig


class EncadreMemConfig(AppConfig):
    name = 'encadre_mem'
