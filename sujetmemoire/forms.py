from django.contrib.auth.forms import UserCreationForm
from django import forms
from django.db import transaction
from .models import Sujetmemoire
from django.forms import MultiWidget, TextInput


class SujetmemoireEditForm(forms.ModelForm):
   # title = forms.CharField(label='Titre du mémoire',widget=forms.Textarea(
    #    attrs={"rows":2,"placeholder":"titre de votre mémoire"}),required=False)
    class Meta:
        model = Sujetmemoire
        fields = ('domaine','titre_sujet','prerequis', 'disponibilite','pdf')
