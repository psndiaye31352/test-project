from django.db import models
from account.models import User, Prof

class Sujetmemoire(models.Model):
    domaine = models.CharField(max_length=200, blank=True)
    titre_sujet = models.CharField(max_length=300, blank=True)
    prerequis = models.CharField(max_length=200, blank=True)
    prof = models.ForeignKey(Prof, on_delete=models.CASCADE,default=None)
    uploaded_at = models.DateTimeField(auto_now=True, blank=True)
    disponibilite = models.CharField(max_length=50, blank=True)
    pdf = models.FileField(upload_to='sujet_memoire/pdfs/', blank=True)
    

