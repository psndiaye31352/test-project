from django.shortcuts import redirect, render
from account.models import User, Prof,Etudiant, Alumni
from .models import Sujetmemoire, Prof
from .forms import  SujetmemoireEditForm
import sweetify 
from django.contrib import messages
from django.contrib.auth.decorators import login_required

# Poster un sujet de mémoire.

def create_sujet(request):
    if request.method == 'POST': 
            profile_forms = SujetmemoireEditForm(request.POST,request.FILES)
            if  profile_forms.is_valid():
                instance = profile_forms.save(commit=False)
                instance.prof = request.user.prof
                instance.save()
                sweetify.success(request , 'le sujet a été ajouté avec succès')
            else:
                sweetify.error(request, 'Une erreur est survenue', persistent='Vérifier')
    else:
            profile_forms= SujetmemoireEditForm()
    return render(request,
                    'sujetmemoire/create_sujet.html',
                    {'profile_forms': profile_forms})

#liste des sujet de mémoire ajoutés par les enseignants
def list_sujet(request):
    sujets = Sujetmemoire.objects.all().order_by('-uploaded_at') 
    return render(request, 'sujetmemoire/list_sujet.html', {
        'sujets': sujets
    })

#liste des sujet de mémoire ajoutés par un enseignant
def list_sujet_id(request, prof):
    sujets = Sujetmemoire.objects.filter(prof=prof)
    return render(request, 'sujetmemoire/list_sujet_id.html', {
        'sujets': sujets
    })

@login_required
def delete_sujet(request, id):
    sujets = Sujetmemoire.objects.get(id=id)
    sujets.delete()
   # messages.error(request, 'User was deleted successfully!')
    return redirect('list_sujet_id')

@login_required
def delete_sujetBis(request, id):
    sujets = Sujetmemoire.objects.get(id=id)
    sujets.delete()
    messages.error(request, 'User was deleted successfully!')
    return redirect('list_sujet')

@login_required
def view_sujet(request, id):
    sujets = Sujetmemoire.objects.filter(id=id)
    return render(request,'sujetmemoire/view_sujet.html', {'sujets': sujets})
 