from django.urls import path, include
from . import views



urlpatterns = [
# post views
#path('list_sujet_id/', views.list_sujet_id, name='list_sujet_id'),
path('list_sujet/list_sujet_id/<str:prof>', views.list_sujet_id, name='list_sujet_id'),
path('list_sujet/', views.list_sujet, name='list_sujet'),
path('list_sujet/view_sujet/<int:id>', views.view_sujet, name='view_sujet'),
#path('list_etudiant/view_etudiant/<int:id>', views.view_etudiant, name='view_etudiant'),
path('create_sujet/', views.create_sujet, name='create_sujet'),
#supprimer un sujet de mémoire
path('list_sujet/list_sujet_id/<str:prof>/delete_sujet/<int:id>', views.delete_sujet, name='delete_sujet'),
path('list_sujet/delete_sujetBis/<int:id>', views.delete_sujetBis, name='delete_sujetBis'),

]
