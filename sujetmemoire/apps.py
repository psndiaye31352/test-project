from django.apps import AppConfig


class SujetmemoireConfig(AppConfig):
    name = 'sujetmemoire'
