from django.contrib.auth.forms import UserCreationForm
from django import forms
from django.db import transaction
from .models import User,Etudiant,Prof, Alumni
from django.forms import MultiWidget, TextInput

class EtudiantSignUpForm(UserCreationForm):
    username = forms.CharField(label='Nom d\'utilisateur',
                               widget=forms.TextInput)
    first_name = forms.CharField(label='Prénom',
                               widget=forms.TextInput)
    last_name = forms.CharField(label='Nom',
                                widget=forms.TextInput)
    password2 = forms.CharField(label='Confirmer votre mot de passe',
                                widget=forms.PasswordInput)

    class Meta(UserCreationForm.Meta):
        model = User
        fields = ('username','first_name','last_name', 'email')

    @transaction.atomic
    def save(self):
        user = super().save(commit=False)
        user.is_etudiant = True
        user.save()
        etudiant = Etudiant.objects.create(user=user)
        alumni = Alumni.objects.create(etudiant=etudiant)
        #prof.phone_number=self.cleaned_data.get('phone_number')
        #prof.designation=self.cleaned_data.get('designation')
        etudiant.save()
        alumni.save()
        return user

        

class ProfSignUpForm(UserCreationForm):
    username = forms.CharField(label='Nom d\'utilisateur',
                               widget=forms.TextInput)
    first_name = forms.CharField(label='Prénom',
                               widget=forms.TextInput)
    last_name = forms.CharField(label='Nom',
                                widget=forms.TextInput)
    password2 = forms.CharField(label='Confirmer votre mot de passe',
                                widget=forms.PasswordInput)

    class Meta(UserCreationForm.Meta):
        model = User
        fields = ('username','first_name','last_name', 'email')

    @transaction.atomic
    def save(self):
        user = super().save(commit=False)
        user.is_prof = True
        user.is_staff = True 
        user.save()
        prof = Prof.objects.create(user=user)
        #prof.phone_number=self.cleaned_data.get('phone_number')
        #prof.designation=self.cleaned_data.get('designation')
        prof.save()
        return user



class UserEditForm(forms.ModelForm):
    first_name = forms.CharField(label='Prénom',
                               widget=forms.TextInput)
    last_name = forms.CharField(label='Nom',
                               widget=forms.TextInput)
    class Meta:
        model = User
        fields = ('first_name', 'last_name', 'email')


class EtudiantEditForm(forms.ModelForm):
    num_cart = forms.CharField(label='Numéro de carte d\'étudiant',
                               widget=forms.TextInput,required=False)
    phone_number = forms.CharField(label='Numéro de téléphone',
                               widget=forms.TextInput,required=False)

    class Meta:
        model = Etudiant
        fields = ('is_alumni','num_cart','phone_number','niveau','promotion','specialite','domaine_predilection', 'photo')

class AlumniEditForm(forms.ModelForm):
    title = forms.CharField(label='Titre du mémoire',widget=forms.Textarea(
        attrs={"rows":2,"placeholder":"titre de votre mémoire"}),required=False)
    date_soutenance = forms.DateField(label='Date de la soutenance',widget=forms.DateInput(format='%d/%m/%Y'),required=False)
    class Meta:
        model = Alumni
        fields = ('situation','entreprise','poste','title','date_soutenance','taille_fichier','pdf', 'couverture')

class ProfEditForm(forms.ModelForm):
    contenu_recherche = forms.CharField(label='Contenu',widget=forms.Textarea(
        attrs={"rows":6,"placeholder":"résumé de votre projet de recherche"}),required=False)
    phone_number = forms.CharField(label='Numéro de téléphone',
                               widget=forms.TextInput,required=False)
    num_matricule = forms.CharField(label='Numéro de matricule',widget=forms.TextInput,required=False)
    class Meta:
        model = Prof
        fields = ('phone_number','grade','type_prof','specialite','domaine_recherche','contenu_recherche','num_matricule','bureau', 'photo')
