from django.contrib import admin
from .models import User, Etudiant, Prof, Alumni

admin.site.register(User)
admin.site.register(Etudiant)
admin.site.register(Prof)
admin.site.register(Alumni)
