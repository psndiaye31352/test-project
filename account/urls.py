from django.urls import path
from . import views
from django.contrib.auth import views as auth_views


urlpatterns = [
# post views
#path('login/', views.login, name='login'),
path('login/', auth_views.LoginView.as_view(), name='login'),
path('logout/', auth_views.LogoutView.as_view(), name='logout'),
path('', views.dashboard, name='dashboard'),
path('calendrier/', views.calendar, name='calendar'),

# change password urls
path('password_change/',auth_views.PasswordChangeView.as_view(),name='password_change'),
path('password_change/done/',auth_views.PasswordChangeDoneView.as_view(),name='password_change_done'),


# reset password urls
path('password_reset/',auth_views.PasswordResetView.as_view(),name='password_reset'),
path('password_reset/done/',auth_views.PasswordResetDoneView.as_view(),name='password_reset_done'),
path('reset/<uidb64>/<token>/',auth_views.PasswordResetConfirmView.as_view(),name='password_reset_confirm'),
path('reset/done/',auth_views.PasswordResetCompleteView.as_view(),name='password_reset_complete'),

#path to register
path('etudiant_register/',views.etudiant_register.as_view(), name='etudiant_register'),
path('prof_register/',views.prof_register.as_view(), name='prof_register'),
path('register/',views.register, name='register'),
path('register_done/',views.register_done, name='register_done'),

#path pour éditer le profil
path('edit/', views.edit, name='edit'),
#permets à l'administrateur de supprimer des enseignants et étudiants
path('gerer_etudiant/', views.gerer_etudiant, name='gerer_etudiant'),
path('gerer_etudiant/delete_etudiant/<int:id>', views.delete_etudiant, name='delete_etudiant'),
path('gerer_prof/', views.gerer_prof, name='gerer_prof'),
path('gerer_prof/delete_prof/<int:id>', views.delete_prof, name='delete_prof'),

#les vues pour les alumnis
path('list_alumni/', views.list_alumni, name='list_alumni'),
path('edit_alumni/', views.edit_alumni, name='edit_alumni'),
path('list_alumni/view_alumni/<int:id>', views.view_alumni, name='view_alumni'),

#vues pour l'ajout des mémoires par les alumnis
path('list_memoire/', views.list_memoire, name='list_memoire'),
]