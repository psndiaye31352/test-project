from django.db import models
from django.contrib.auth.models import AbstractUser
from django.urls import reverse 




class User(AbstractUser):
    is_etudiant = models.BooleanField(default=False)
    is_prof = models.BooleanField(default=False)
    first_name = models.CharField(max_length=100, blank=True)
    last_name = models.CharField(max_length=100, blank=True)

class Etudiant(models.Model):
    CHOICE = (
    (u"Master 1", u'Master 1'),
    (u"Master 2", u'Master 2'),
    )
    user = models.OneToOneField(User, on_delete = models.CASCADE, primary_key = True)
    num_cart = models.CharField(max_length=20, blank=True)
    phone_number = models.CharField(max_length=20, blank=True)
    niveau = models.CharField(choices=CHOICE,max_length=120, blank=True)
    promotion = models.CharField(max_length=100, blank=True)
    specialite = models.CharField(max_length=100, blank=True)
    domaine_predilection = models.CharField(max_length=100, blank=True)
    photo = models.ImageField(upload_to='users/%Y/%m/%d/', blank=True)
    is_alumni = models.BooleanField(default=False)
    
    def __str__(self):
        return f'Etudiant for user {self.user.username}' 

    def get_absolute_url (self): 
        return reverse ('view_alumni', args = [str ( self.user.id)])



class Alumni(models.Model):
    CHOICE = (
    (u"En recherche d'emploi", u'En recherche d\'emploi'),
    (u"En CDD", u'En CDD'),
    (u"En CDI", u'En CDI'),
    )

    etudiant = models.OneToOneField(Etudiant, on_delete = models.CASCADE, primary_key = True)
    situation = models.CharField(choices=CHOICE,max_length=120, blank=True)
    entreprise = models.CharField(max_length=100, blank=True)
    poste = models.CharField(max_length=100, blank=True)
    title = models.CharField(max_length=500, blank=True)
    date_soutenance = models.DateField(null=True, blank=True)
    taille_fichier = models.CharField(max_length=100, blank=True)
    uploaded_at = models.DateTimeField(auto_now=True, blank=True)
    couverture = models.ImageField(upload_to='memoire/couvertures/', null=True, blank=True)
    pdf = models.FileField(upload_to='memoire/pdfs/', blank=True)
    
        
    def delete(self, *args, **kwargs):
        self.pdf.delete()
        self.couverture.delete()
        super().delete(*args, **kwargs)
    
   

class Prof(models.Model):
    CHOICES = (
    (u"Enseignant chercheur", u'Enseignant chercheur'),
    (u"Intervenant interne", u'Intervenant interne'),
    (u"Intervenant externe", u'Intervenant externe'),
    )
    CHOICESS = (
    (u"Maitre assistant", u'Maitre assistant'),
    (u"Maitre conférence", u'Maitre conférence'),
    (u"Professeur des universités", u'Professeur des universités'),
    (u"ingénieur", u'ingénieur'),
    )
    user = models.OneToOneField(User, on_delete = models.CASCADE, primary_key = True)
    phone_number = models.CharField(max_length=20, blank=True)
    type_prof = models.CharField(max_length=32, choices=CHOICES, blank=True)
    grade = models.CharField(max_length=100, choices=CHOICESS, blank=True)
    specialite = models.CharField(max_length=100, blank=True)
    domaine_recherche = models.CharField(max_length=100, blank=True)
    contenu_recherche = models.CharField(max_length=1000, blank=True)
    num_matricule = models.CharField(max_length=100, blank=True)
    bureau = models.CharField(max_length=100, blank=True)   
    photo = models.ImageField(upload_to='users/%Y/%m/%d/', blank=True)
    def __str__(self):
        return f'Prof for user {self.user.username}' 
    
    def get_absolute_url (self): 
        return reverse ('view_prof', args = [str ( self.user.id)])
    