from django.contrib.auth import login, logout,authenticate
from django.shortcuts import redirect, render
from django.contrib import messages
from django.contrib.auth.decorators import login_required
from django.views.generic import CreateView
from .forms import EtudiantSignUpForm, ProfSignUpForm
from django.contrib.auth.forms import AuthenticationForm
from .models import User, Prof,Etudiant, Alumni
from .forms import  UserEditForm, EtudiantEditForm, ProfEditForm, AlumniEditForm
import  sweetify 

def register(request):
    return render(request, 'account/register.html')

class etudiant_register(CreateView):
    model = User
    form_class = EtudiantSignUpForm
    template_name = 'account/etudiant_register.html'

    def form_valid(self, form):
        user = form.save()
        login(self.request, user)
        return redirect('register_done')

class prof_register(CreateView):
    model = User
    form_class = ProfSignUpForm
    template_name = 'account/prof_register.html'

    def form_valid(self, form):
        user = form.save()
        login(self.request, user)
        return redirect('register_done')


def login_request(request):
    if request.method=='POST':
        form = AuthenticationForm(data=request.POST)
        if form.is_valid():
            username = form.cleaned_data.get('username')
            password = form.cleaned_data.get('password')
            user = authenticate(username=username, password=password)
            if user is not None :
                login(request,user)
                return redirect('account/dashboard.html')
            else:
                messages.error(request,"Invalid username or password")
        else:
                messages.error(request,"Invalid username or password")
    return render(request, 'registration/login.html',
    context={'form':AuthenticationForm()})

def logout_view(request):
    logout(request)
    return redirect('/')


@login_required
def dashboard(request):
    return render(request,
                  'account/dashboard.html',
                  {'dashboard': dashboard})


def calendar(request):
    return render(request,'account/calendar.html',{'calendar': 'calendar'})

def gerer_etudiant(request):
    etudiants = Etudiant.objects.all()
    return render(request,'account/gerer_etudiant.html',{'etudiants': etudiants})

def gerer_prof(request):
    profs = Prof.objects.all()
    return render(request,'account/gerer_prof.html',{'profs': profs})

@login_required
def delete_prof(request, id):
    users = User.objects.get(id=id)
    users.delete()
   # messages.error(request, 'User was deleted successfully!')
    return redirect('gerer_prof')

@login_required
def delete_etudiant(request, id):
    users = User.objects.get(id=id)
    users.delete()
   # messages.error(request, 'User was deleted successfully!')
    return redirect('gerer_etudiant')


def register_done(request):
    return render(request,'account/register_done.html')


def edit(request):
    if request.user.is_prof : # mettre a jour le profil du prof
        if request.method == 'POST':
            user_form = UserEditForm(instance=request.user,
                                    data=request.POST)
            profile_form = ProfEditForm(
                                        instance=request.user.prof,
                                        data=request.POST,
                                        files=request.FILES)
            if user_form.is_valid() and profile_form.is_valid():
                user_form.save()
                profile_form.save()
                sweetify.success(request , 'votre profil a été mis à jour', persistent='OK')
            else:
                sweetify.error(request, 'Une erreur est survenue', persistent='Vérifier')
        else:
            user_form = UserEditForm(instance=request.user)
            profile_form = ProfEditForm(instance=request.user.prof)
        return render(request,
                    'account/edit.html',
                    {'user_form': user_form,
                    'profile_form': profile_form})
    else:
        if request.method == 'POST': #mettre a jour le profil de l'étudiant
            user_form = UserEditForm(instance=request.user,
                                    data=request.POST)
            profile_form = EtudiantEditForm(
                                        instance=request.user.etudiant,
                                        data=request.POST,
                                        files=request.FILES)
            if user_form.is_valid() and profile_form.is_valid():
                user_form.save()
                profile_form.save()
                sweetify.success(request , 'votre profil a été mis à jour')
            else:
                messages.error(request, 'Error updating your profile')
        else:
            user_form = UserEditForm(instance=request.user)
            profile_form = EtudiantEditForm(instance=request.user.etudiant)
        return render(request,
                    'account/edit.html',
                    {'user_form': user_form,
                    'profile_form': profile_form})

#afficher et éditer un alumni
def list_alumni(request):
    etudiants = Etudiant.objects.all()
    
    return render(request,'alumni/list_alumni.html', {'etudiants': etudiants})
   
def edit_alumni(request):
    if request.method == 'POST': #mettre a jour le profil de l'alumni'
            profile_forms = AlumniEditForm(
                                        instance=request.user.etudiant.alumni,
                                        data=request.POST,
                                        files=request.FILES)
            if  profile_forms.is_valid():
                profile_forms.save()
                sweetify.success(request , 'votre profil a été mis à jour')
            else:
                messages.error(request, 'Error updating your profile')
    else:
            profile_forms= AlumniEditForm(instance=request.user.etudiant.alumni)
    return render(request,
                    'alumni/edit_alumni.html',
                    {'profile_forms': profile_forms})

#liste des mémoires ajoutés par les alumnis
def list_memoire(request):
    memoires = Alumni.objects.all()
    return render(request, 'alumni/list_memoire.html', {
        'memoires': memoires
    })


def view_alumni(request, id):
    users = User.objects.filter(id=id)
    return render(request,'alumni/view_alumni.html', {'users': users})

def conto(self):
        album = Etudiant.count()
        return album